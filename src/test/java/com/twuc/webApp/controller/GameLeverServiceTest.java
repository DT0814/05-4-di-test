package com.twuc.webApp.controller;

import com.twuc.webApp.domain.mazeGenerator.AldousBroderMazeAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.DijkstraSolvingAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.Grid;
import com.twuc.webApp.domain.mazeRender.MazeWriter;
import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class GameLeverServiceTest {
    @Autowired
    private AldousBroderMazeAlgorithm mazeAlgorithm;
    @Autowired
    private DijkstraSolvingAlgorithm solvingAlgorithm;
    GameLevelService gameLevelService;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void name() throws IOException {
        //Arrange
        MazeWriter writer = mock(MazeWriter.class);
        when(writer.getName()).thenReturn("color");
        List<MazeWriter> list = new ArrayList<>();
        list.add(writer);
        gameLevelService = new GameLevelService(list, mazeAlgorithm, solvingAlgorithm);

        //Act
        gameLevelService.renderMaze(10, 10, "color");
        //Assert
        verify(writer).render(any(Grid.class), any(OutputStream.class));
    }
}
