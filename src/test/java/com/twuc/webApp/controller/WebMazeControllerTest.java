package com.twuc.webApp.controller;

import com.twuc.webApp.domain.mazeRender.MazeWriter;
import com.twuc.webApp.service.GameLevelService;
import com.twuc.webApp.web.MazeController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class WebMazeControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    private GameLevelService gameLevelService;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void test_return_get_maze() throws Exception {
        //Arrange
        byte[] bytes = new byte[]{1, 2, 3};
        when(gameLevelService.renderMaze(10, 10, "color")).thenReturn(bytes);
        //Act
        mockMvc.perform(get("/buffered-mazes/color"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(bytes));
        //Assert
    }

    @Test
    void test_return_get_maze_2() throws Exception {
        //Arrange
        byte[] bytes = new byte[]{1, 2, 3};
        when(gameLevelService.renderMaze(10, 10, "color")).thenReturn(bytes);
        //Act
        mockMvc.perform(get("/buffered-mazes/color"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(bytes));
        verify(gameLevelService).renderMaze(10, 10, "color");
        //Assert
    }

}

