package com.twuc.webApp.controller;

import com.twuc.webApp.service.GameLevelService;
import com.twuc.webApp.web.MazeController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class MazeControllerTest {

    @Mock
    private GameLevelService gameLevelService;

    private MazeController mazeController;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mazeController = new MazeController(gameLevelService);
    }

    @Test
    void test_get_maze_response() throws IOException {
        //Arrange
        byte[] bytes = new byte[]{0, 1};
        when(gameLevelService.renderMaze(1, 1, "0")).thenReturn(bytes);
        //Act
        ResponseEntity<byte[]> responseEntity = mazeController.getMaze(1, 1, "0");
        //Assert
        Assertions.assertArrayEquals(bytes, responseEntity.getBody());
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertEquals(MediaType.IMAGE_PNG, responseEntity.getHeaders().getContentType());
    }

    @Test
    void test_get_maze2_response() throws IOException {
        //Arrange
        MockHttpServletResponse response = new MockHttpServletResponse();
        //Act
        mazeController.getMaze2(response, 10, 10, "color");
        //Assert
        verify(gameLevelService).renderMaze(response.getOutputStream(), 10, 10, "color");
        Assertions.assertEquals(MediaType.IMAGE_PNG_VALUE, response.getContentType());
    }

    @Test
    void test_get_maze2_response_2() throws IOException {
        //Arrange
        MockHttpServletResponse response = new MockHttpServletResponse();
        //Act
        mazeController.getMaze2(response, 10, 10, "color");
        //Assert
        doAnswer(invocation -> {
            Assertions.assertSame(response.getOutputStream(), invocation.getArgument(0));
            Assertions.assertNull(invocation.getArgument(1));
            Assertions.assertEquals(Integer.valueOf(10), invocation.getArgument(1));
            Assertions.assertEquals(Integer.valueOf(10), invocation.getArgument(2));
            Assertions.assertEquals("color", invocation.getArgument(3));
            Assertions.assertEquals(MediaType.IMAGE_PNG_VALUE, response.getContentType());
            return null;
        }).when(gameLevelService).renderMaze(any(response.getOutputStream().getClass()), anyInt(), anyInt(), anyString());

    }
}
