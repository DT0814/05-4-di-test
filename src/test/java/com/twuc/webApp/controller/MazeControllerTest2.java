package com.twuc.webApp.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MazeControllerTest2 {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void test_return_get_maze_2() throws Exception {
        //Arrange
        //Act
        MvcResult result = mockMvc.perform(get("/buffered-mazes/color"))
                .andExpect(status().isOk())
                .andReturn();

        Assertions.assertArrayEquals(
                Arrays.copyOfRange(result.getResponse().getContentAsByteArray(), 0, 8),
                new byte[]{(byte) 137, 80, 78, 71, 13, 10, 26, 10});
        //Assert
    }
}
