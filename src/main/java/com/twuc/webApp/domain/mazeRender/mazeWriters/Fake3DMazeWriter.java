package com.twuc.webApp.domain.mazeRender.mazeWriters;

import com.twuc.webApp.domain.mazeGenerator.Grid;
import com.twuc.webApp.domain.mazeRender.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public abstract class Fake3DMazeWriter extends MazeWriter {
    protected Fake3DMazeWriter(MazeComponentFactory factory) {
        super(factory);
    }

    private int translateCoordWithMargin(int original) {
        return original + settings.getMargin();
    }

    private int calculateWidth(RenderGrid renderGrid) {
        return settings.getCellSize() / 2 * (renderGrid.getRowCount() + renderGrid.getColumnCount()) + settings.getMargin() * 2;
    }

    private int calculateHeight(RenderGrid renderGrid) {
        return settings.getCellSize() / 4 * (renderGrid.getRowCount() + renderGrid.getColumnCount()) + settings.getMargin() * 2;
    }

    @Override
    public void render(Grid grid, OutputStream stream) throws IOException {
        RenderGrid renderGrid = new RenderGrid(grid);
        BufferedImage image = createImage(renderGrid);
        Graphics2D context = image.createGraphics();
        renderBackground(context, image.getWidth(), image.getHeight());
        renderMaze(renderGrid, context);
        ImageIO.write(image, "png", stream);
    }

    private void renderMaze(RenderGrid renderGrid, Graphics2D context) {
        for (int columnIndex = renderGrid.getColumnCount() - 1; columnIndex >= 0; --columnIndex)
        {
            for (int rowIndex = 0; rowIndex < renderGrid.getRowCount(); ++rowIndex)
            {
                RenderCell cell = renderGrid.getCell(rowIndex, columnIndex);
                Rectangle cellArea = calculateCellArea(renderGrid, cell);
                renderGround(context, cell, cellArea);
                renderWall(context, cell, cellArea);
            }
        }
    }

    private Rectangle calculateCellArea(RenderGrid renderGrid, RenderCell cell) {
        return new Rectangle(
            translateCoordWithMargin((cell.getColumn() + cell.getRow()) * settings.getCellSize() / 2),
            translateCoordWithMargin((renderGrid.getColumnCount() - cell.getColumn() - 1 + cell.getRow()) * settings.getCellSize() / 4),
            settings.getCellSize(),
            settings.getCellSize() / 2);
    }

    private void renderWall(Graphics2D context, RenderCell cell, Rectangle cellArea) {
        renderCell(context, cell, cellArea, RenderType.WALL, wallRenders);
    }

    private void renderGround(Graphics2D context, RenderCell cell, Rectangle cellArea) {
        renderCell(context, cell, cellArea, RenderType.GROUND, groundRenders);
    }

    private void renderCell(
        Graphics2D context,
        RenderCell cell,
        Rectangle cellArea,
        RenderType renderType,
        List<CellRender> renders) {
        if (cell.getRenderType() != renderType) { return; }
        for (CellRender render : renders) {
            render.render(context, cellArea, cell);
        }
    }

    private void renderBackground(Graphics2D context, int width, int height) {
        Rectangle fullArea = new Rectangle(0, 0, width, height);
        for (AreaRender backgroundRenderer : backgroundRenders)
        {
            backgroundRenderer.render(context, fullArea);
        }
    }

    private BufferedImage createImage(RenderGrid renderGrid) {
        int width = calculateWidth(renderGrid);
        int height = calculateHeight(renderGrid);
        return new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
    }
}
