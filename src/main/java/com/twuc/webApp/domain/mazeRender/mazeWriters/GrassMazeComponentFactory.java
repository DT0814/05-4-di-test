package com.twuc.webApp.domain.mazeRender.mazeWriters;

import com.twuc.webApp.domain.mazeRender.AreaRender;
import com.twuc.webApp.domain.mazeRender.CellRender;
import com.twuc.webApp.domain.mazeRender.MazeComponentFactory;
import com.twuc.webApp.domain.mazeRender.MazeRenderSettings;
import com.twuc.webApp.domain.mazeRender.areaRenders.AreaTileImageRender;
import com.twuc.webApp.domain.mazeRender.cellRenders.DirectedImageCellRender;
import com.twuc.webApp.domain.mazeRender.cellRenders.RandomizedImageCellRender;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.twuc.webApp.domain.mazeRender.mazeWriters.ResourceLoader.loadResource;

@Component
public class GrassMazeComponentFactory implements MazeComponentFactory {
    private static String getGrassResourcePath(String path) { return "images/grass/" + path; }

    private CellRender createGrassDartRenderer() {
        return new DirectedImageCellRender(
            loadResource(getGrassResourcePath("grass_sm_dart_south.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_north.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_east.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_west.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_north_south.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_north_east.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_north_west.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_south_east.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_south_west.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_east_west.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_north_south_east.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_north_south_west.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_north_east_west.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_south_east_west.png")),
            loadResource(getGrassResourcePath("grass_sm_dart_north_south_east_west.png")));
    }

    private CellRender createGrassWallRenderer() {
        return new DirectedImageCellRender(
            loadResource(getGrassResourcePath("sm_grass_wall_north.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_south.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_east.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_west.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_north_south.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_north_east.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_north_west.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_south_east.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_south_west.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_east_west.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_south_east.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_south_west.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_east_west.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_south_east_west.png")),
            loadResource(getGrassResourcePath("sm_grass_wall_south_east_west.png")));
    }

    private CellRender createGrassGroundRender() {
        List<BufferedImage> textures = Arrays.asList(
            loadResource(getGrassResourcePath("sm_grass_ground_1.png")),
            loadResource(getGrassResourcePath("sm_grass_ground_2.png")),
            loadResource(getGrassResourcePath("sm_grass_ground_3.png")),
            loadResource(getGrassResourcePath("sm_grass_ground_4.png")),
            loadResource(getGrassResourcePath("sm_grass_ground_5.png")),
            loadResource(getGrassResourcePath("sm_grass_ground_6.png"))
        );
        return new RandomizedImageCellRender(textures);
    }

    @Override
    public List<AreaRender> createBackgroundRenders() {
        return Collections.singletonList(
            new AreaTileImageRender(loadResource(getGrassResourcePath("sm_grass_ground_1.png"))));
    }

    @Override
    public List<CellRender> createWallRenders() {
        return Collections.singletonList(createGrassWallRenderer());
    }

    @Override
    public List<CellRender> createGroundRenders() {
        return Arrays.asList(
            createGrassGroundRender(),
            createGrassDartRenderer()
        );
    }

    @Override
    public MazeRenderSettings createSettings() {
        return new MazeRenderSettings(32, 20);
    }
}
