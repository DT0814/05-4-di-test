package com.twuc.webApp.domain.mazeRender.mazeWriters;

import org.springframework.stereotype.Component;

@Component
public class ColorSolvingMazeWriter extends NormalMazeWriter {
    public ColorSolvingMazeWriter(ColorSolvingMazeComponentFactory factory) {
        super(factory);
    }

    @Override
    public String getName() {
        return "color-solve";
    }
}
